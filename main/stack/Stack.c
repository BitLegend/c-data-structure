#include <stdio.h>
#include <stdlib.h>

// 栈节点元素
typedef struct Node
{
    /* data */
    int val;

    struct Node *next;
    struct Node *prev;
} Node;

// 头节点元素
Node *head = NULL;
// 记录栈元素的数量
int size = 0;

void init()
{
    head = NULL;
    size = 0;
}

void push(int val)
{
    Node *node = (Node *)malloc(sizeof(Node));
    node->val = val;
    node->prev = node->next = NULL;
    if (head == NULL)
    {
        head = node;
    }
    else
    {
        head->next = node;
        node->prev = head;
        head = node;
    }
    size++;
}

int pop()
{
    if (head == NULL)
    {
        return 0xFFFFFF;
    }
    Node *old = head;
    head = head->prev;
    if (head != NULL)
    {
        head->next = NULL;
    }

    old->prev = NULL;
    int val = old->val;
    size--;
    free(old);
    return val;
}

int stackSize()
{
    return size;
}

int main()
{
    init();
    push(1);
    push(2);
    printf("%d\n", pop());
    printf("%d\n", pop());
    printf("%d", stackSize());
    return 0;
}