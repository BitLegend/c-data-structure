#include <stdio.h>
#include <stdlib.h>

typedef struct MinStack
{
    /* data */
    int len;     // 元素长度
    int index;   // 新元素下标
    int *values; // 值数组
    int *min;    // 最值数组
} MinStack;

// 初始化
MinStack *init(int size)
{
    // 申请空间
    MinStack *stack = malloc(sizeof(MinStack));
    // 初始化值
    stack->index = 0;
    stack->len = 0;
    stack->values = malloc(sizeof(int) * size);
    stack->min = malloc(sizeof(int) * size);
    return stack;
}

void push(MinStack *stack, int val)
{
    // 指针为空直接返回
    if (stack == NULL)
    {
        return;
    }
    // 将元素进行保存
    stack->values[stack->index] = val;
    // 判断最值
    if (stack->len > 0)
    {
        // 存在元素就与当前栈的最值进行比较
        int lastMin = stack->min[stack->index - 1];
        if (lastMin < val)
        {
            stack->min[stack->index] = lastMin;
        }
        else
        {
            stack->min[stack->index] = val;
        }
    }
    else
    {
        // 栈为空直接设置为最值
        stack->min[stack->index] = val;
    }
    stack->index++;
    stack->len++;
}

int pop(MinStack *stack)
{
    // 栈为空
    if (stack == NULL || stack->len < 1)
    {
        return 0xFFFFFF;
    }
    // 出栈
    int val = stack->values[stack->index - 1];
    // 更新下标和长度
    stack->len--;
    stack->index--;
    return val;
}

int min(MinStack *stack)
{
    // 直接返回最值
    return stack->min[stack->index - 1];
}

// 销毁栈对象
void destory(MinStack *stack)
{
    //释放空间 重置数据
    stack->index = stack->len = 0;
    free(stack->min);
    free(stack->values);
}

int main()
{
    MinStack *stack = init(100);
    push(stack, 1);
    printf("min=%d\n", min(stack));
    push(stack, -1);
    push(stack, 100);
    printf("min=%d\n", min(stack));
    printf("pop=%d\n", pop(stack));
    printf("len=%d\n", stack->len);
    return 0;
}