#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    // 节点的值
    int val;
    // 节点的在一个节点
    struct Node *next;
} Node;

typedef struct Queue
{
    /* data */
    // 队列的头节点
    Node *head;
    // 队列的尾节点
    Node *tail;
    // 队列的元素数量
    int len;
} Queue;

//  初始化队列
Queue *init()
{
    // 申请空间
    Queue *q = malloc(sizeof(Queue));
    // 初始化成员属性
    q->head = q->tail = NULL;
    q->len = 0;
    return q;
}

// 元素入队列
void push(Queue *q, int val)
{
    // 申请空间
    Node *node = malloc(sizeof(Node));
    node->val = val;
    node->next = NULL;
    // 判断队列是不是为空
    if (q->len == 0)
    {
        // 直接赋值
        q->head = q->tail = node;
    }
    else
    {
        // 将元素添加至队列末尾
        q->tail->next = node;
        q->tail = node;
    }
    // 长度 + 1
    q->len++;
}

// 元素出队列
int pop(Queue *q)
{
    if (q->len > 0)
    {
        // 获取头节点的值
        Node *h = q->head;
        int val = h->val;
        // 没有其他元素重置为NULL
        if (h->next == NULL)
        {
            q->head = q->tail = NULL;
        }
        else
        {
            q->head = h->next;
        }
        // 释放节点空间
        free(h);
        q->len--;
        return val;
    }
    return 0xFFFFFF;
}

// 返回队列的长度
int size(Queue *q)
{
    if (q == NULL)
    {
        return -1;
    }
    return q->len;
}

void printQueue(Queue *q)
{
    if (q != NULL)
    {
        Node *h = q->head;
        while (h != NULL)
        {
            printf("%d->", h->val);
            h = h->next;
        }
        printf("\n");
    }
}

// 销毁队列
void destory(Queue *q)
{
    if (q != NULL)
    {
        // 逐个释放节点的元素
        Node *h = q->tail;
        while (h != NULL)
        {
            free(h);
            h = h->next;
        }
        // 释放队列
        free(q);
    }
}

int main()
{
    Queue *queue = init();
    push(queue, 10);
    push(queue, 11);
    push(queue, 12);
    push(queue, 13);
    push(queue, 14);
    printQueue(queue);
    printf("%d\n", size(queue));
    printf("%d\n", pop(queue));
    printf("%d\n", size(queue));
    printQueue(queue);
    destory(queue);
    return 0;
}