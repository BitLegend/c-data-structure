#include <stdio.h>
#include <stdlib.h>

typedef struct ListNode
{
    struct ListNode *last;
    struct ListNode *next;
    int val;
} ListNode;

void init(ListNode **head)
{
    *head = (ListNode *)malloc(sizeof(ListNode));
    (*head)->last = NULL;
    (*head)->next = NULL;
    (*head)->val = -1;
}

void insertNode(ListNode *head, int val, int index)
{
    if (head == NULL || index < 0)
    {
        return;
    }
    ListNode *h = head;
    //定位到要新元素的前一个位置
    while (index > 0 && h->next != NULL)
    {
        h = h->next;
        index--;
    }
    ListNode *node = (ListNode *)malloc(sizeof(ListNode));
    node->val = val;
    node->next = h->next;
    // 如果当前位置之后还有元素，更新下个元素的指向
    if(node->next != NULL){
        h->next->last = node;
    }
    node->last = h;
    h->next = node;
}

void deleteNode(ListNode *head, int index)
{
    if (head == NULL || index < 0)
    {
        return;
    }
    ListNode *h = head;
    //定位到要删除的元素的前一个位置
    while (index > 0 && h->next != NULL)
    {
        h = h->next;
        index--;
    }
    if (h->next != NULL)
    {
        //保存要删除的元素 释放空间
        ListNode *next = h->next;
        // 如果要函删除的元素之后还有元素 更新指向
        if(h->next->next != NULL){
            h->next->next->last = h;
        }
        h->next = h->next->next;
        free(next);
    }
}

void printList(ListNode *head)
{
    if (head == NULL)
    {
        printf("empty list\n");
        return;
    }
    ListNode *h = head;
    while (h->next != NULL)
    {
        printf("%d -> ", h->val);
        h = h->next;
    }
    printf("%d\n", h->val);
}

int main()
{
    ListNode *head = NULL;
    init(&head);
    int arr[] = {1, 2, 3, 4, 5};
    for (int i = 0; i < 5; i++)
    {
        //每次都插入在末尾
        insertNode(head, arr[i], i);
    }
    printList(head->next);
    // 从 0 开始
    deleteNode(head, 4);
    printList(head->next);
    deleteNode(head, 2);
    printList(head->next);
    deleteNode(head, 0);
    printList(head->next);
    deleteNode(head, 0);
    printList(head->next);
    deleteNode(head, 0);
    printList(head->next);
    return 0;
}