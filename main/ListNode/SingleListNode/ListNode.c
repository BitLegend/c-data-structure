#include <stdio.h>
#include <stdlib.h>

typedef struct ListNode
{
	struct ListNode *next;
	int val;
} ListNode;

void init(ListNode **head)
{
	if (head == NULL)
	{
		return;
	}
	//为什么这里是ListNode ** head?
	*head = (ListNode *)malloc(sizeof(ListNode));
	(*head)->next = NULL;
	(*head)->val = -1;
}

void insertNode(ListNode *head, int val, int index)
{
	if (head == NULL)
	{
		return;
	}
	ListNode *h = head;
	while (index > 0 && h->next != NULL)
	{
		h = h->next;
		index--;
	}
	ListNode *node = (ListNode *)malloc(sizeof(ListNode));
	node->val = val;
	//开始插入数据
	node->next = h->next;
	h->next = node;
}

ListNode *reverseList(ListNode *head)
{
	if (head == NULL)
	{
		return NULL;
	}
	ListNode *node = head;
	// 获取新的链表头
	ListNode *new = reverseList(head->next);
	// 新链表节点为NULL返回当前节点
	if (new == NULL)
	{
		return node;
	}
	// 将当前节点添加到链表的末尾
	ListNode *t = new;
	while (t != NULL && t->next != NULL)
	{
		t = t->next;
	}
	node->next = NULL;
	t->next = node;
	// 返回链表头
	return new;
}

void deleteNode(ListNode *head, int index)
{
	if (head == NULL)
	{
		return;
	}
	ListNode *h = head;
	while (index > 0 && h->next != NULL)
	{
		h = h->next;
		index--;
	}
	//确保是删除最后一个元素
	if (h->next != NULL)
	{
		ListNode *next = h->next;
		h->next = h->next->next;
		// 记得释放空间
		free(next);
	}
}

void printList(ListNode *head)
{
	if (head == NULL)
	{
		printf("empty list\n");
		return;
	}
	ListNode *h = head;
	while (h->next != NULL)
	{
		printf("%d -> ", h->val);
		h = h->next;
	}
	printf("%d\n", h->val);
}

int main()
{
	ListNode *head = NULL;
	init(&head);
	int arr[] = {1, 2, 3, 4, 5};
	for (int i = 0; i < 5; i++)
	{
		//每次都插入在末尾
		insertNode(head, arr[i], i);
	}
	printList(head->next);
	// 从 0 开始

	head->next = reverseList(head->next);
	printList(head->next);
	return 0;
}

/**
 * 为什么这里是ListNode ** head?
 * 
 * 首先我们在main函数中定义的是LisNode * head,这个是指针
 * 如果我们函数传参的时候传递的是 head 也就是 ListNode * 类型的
 * 那我们的init函数中的head实际上是 main.head 的拷贝
 * main.head.address != init.head.address （两个变量的地址不相等，后续指向发生改变也不会同步）
 * 当我们分配空间之后 init.head 指向的是我们的申请分配的空间
 * 而 main.head 依然是NULL
 * 当传入的是ListNode ** 时，init.head 指向的是main.head 的地址
 * 当*init.head 指向新分配的空间时，main.head 的指向也发生了改变
 * 一个原则 ： 当你想改变一个变量的值时，就传递它的地址
 */

//https://gitlab.com/BitLegend/c-data-structure.git